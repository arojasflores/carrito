<%-- 
    Document   : index
    Created on : 20-feb-2017, 11:04:45
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrito de la Compra (Versi&oacute;n Gen&eacute;rica)</title>
    </head>
    <body>
        <form action="carrito.jsp" method="POST">
            <h3>Por favor, seleccione que producto quiere agregar o eliminar del carrito</h3> 
            <h4>Agregar producto</h4>
        
            <select id="elemento" name="elemento">
                <optgroup label="cat1">
                    <option value="A1&amp;5">Producto A1 (2.50 e)</option>
                    <option value="B1&amp;7">Producto B1 (3.50 e)</option>
                    <option value="C1&amp;4">Producto C1 (2.00 e)</option>
                </optgroup>
                <optgroup label="cat2">
                    <option value="A2&amp;9">Producto A2 (4.50 e)</option>
                    <option value="B2&amp;12">Producto B2 (6.00 e)</option>
                    <option value="C2&amp;10">Producto C2 (5.00 e)</option>
                    <option value="D2&amp;15">Producto D2 (7.50 e)</option>
                </optgroup>
                <optgroup label="cat3">
                    <option value="A3&amp;20">Producto A3 (10.00 e)</option>
                    <option value="B3&amp;30">Producto B3 (15.00 e)</option>
                    <option value="C3&amp;60">Producto C3 (30.00 e) </option>
                </optgroup>
            </select>
            <input type="submit" name="add" value="Agregar">
            <input type="submit" name="remove" value="Eliminar">
            <input type="submit" name="clear" value="Eliminar todo">
        </form>
    </body>
</html>
