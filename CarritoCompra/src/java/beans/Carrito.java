/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.*;
import javax.servlet.http.*;

/**
 *
 * @author elcer
 */
public class Carrito {
    private final Map<String, Integer> carrito = new HashMap<>();
    private final Map<String, Double> precios = new HashMap<>();
    private double precio;
    
    private double total = 0;
    
    private String item, accion;
            
    private void agregarProducto(String producto, double precio) {
        if (carrito.containsKey(producto)) {
            int cantidadP = carrito.get(producto);
            carrito.remove(producto);
            carrito.put(producto, cantidadP+1);
        } else carrito.put(producto, 1);
        
        if (!precios.containsKey(producto)) precios.put(producto, precio);
        
        int cantidadProducto = carrito.get(producto);
        
        total += cantidadProducto*precio;
        
    }
    
    private void eliminarProducto(String producto) {
        if (carrito.containsKey(producto)) {
            int cantidadP = carrito.get(producto);
            if (cantidadP > 1) carrito.replace(producto, cantidadP-1);
            else carrito.remove(producto);
        } 
    }
    
    public void setAccion(String accion) {
        this.accion = accion;
    }
    
    public void setProducto(String cadenaProducto) {
        String[] arrayCadena1 = cadenaProducto.split("\\&"); //( nombre, precio+e )
        item = arrayCadena1[0];
        precio = Double.parseDouble(arrayCadena1[1])/2;
    }
    
    public String[][] getProductos() {
        Set<String> ss = carrito.keySet();
        String[][] productos = new String[ss.size()][4];
        Iterator<String> it = ss.iterator();
        
        int i = 0;
        
        while (it.hasNext()) {
            String productoActual = it.next();
            
            int cantidad = carrito.get(productoActual);
            double p = precios.get(productoActual);
            double sub = p*cantidad;
            
            productos[i][0] = "Concepto: " + productoActual + "<br/>"; 
            productos[i][1] = "Precio/Unidad: " + p + " e/ud<br/>";
            productos[i][2] = "Cantidad: " + cantidad + " uds<br/>";
            productos[i][3] = "Subtotal: " + sub + " e<br/><br/>";

            i++;
        }
        
        return productos;
    }
    
    public double precioTotal() {
        return total;
    }
    
    private void eliminarTodo() {
        carrito.clear();
        total = 0;
    }
    
    public void processRequest(HttpServletRequest request) {
        if (accion == null || accion.equals("agregar")) 
            agregarProducto(item, precio);
        if (accion.equals("eliminar"))
            eliminarProducto(item);
        if (accion.equals("limpiar"))
            eliminarTodo();
        
        precio = 0;
        accion = null;
        item = null;
    }        
}
