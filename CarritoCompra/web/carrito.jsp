<%-- 
    Document   : carrito
    Created on : 20-feb-2017, 11:11:46
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="carrito" scope="session" class="beans.Carrito" />
    </head>
    
    <jsp:setProperty name="carrito" property="*" />
    <%
        if (request.getParameter("elemento") != null) 
            carrito.setProducto(request.getParameter("elemento"));
        
        if (request.getParameter("add") != null)
            carrito.setAccion("agregar");
        if (request.getParameter("remove") != null)
            carrito.setAccion("eliminar");
        if (request.getParameter("clear") != null) 
            carrito.setAccion("limpiar");
        
        carrito.processRequest(request);
        
    %>
    <h3>Tu carrito de la compra: </h3>
    <%
        String[][] cps = carrito.getProductos();
        if (cps.length > 0) {
            String msg = "";
            for (int i = 0; i < cps.length; i++) {
                for (int j = 0; j < 4; j++) {
                    msg += cps[i][j];
                }
            }
            out.println(msg + "<br/>");
            out.println("-----------------------------------------<br/>");
            out.println("Total: " + carrito.precioTotal() + " e<br/>");
        } else out.println("No hay resultados<br/><br/>");

    %><br/><br/>
    <a href="index.jsp">Volver Atr&aacute;s</a><br/><br/>
</html>
